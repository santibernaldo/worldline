//
//  WLLocation.m
//  wordLineTest
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 Santi Bernaldo. All rights reserved.
//

#import "WLLocation.h"

@implementation WLLocation

+ (JSONKeyMapper*)keyMapper {
    NSDictionary *map = @{@"id" : @"idPoint",
                          @"title" : @"titlePoint",
                          @"geocoordinates" : @"geocoordinates",
                          @"latitude" : @"latitude",
                          @"longitude" : @"longitude"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    if ([propertyName isEqualToString:@"latitude"]
        ||[propertyName isEqualToString:@"longitude"])
    {
        return YES;
    }
    return NO;
}

@end
