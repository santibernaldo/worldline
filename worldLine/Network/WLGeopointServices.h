//
//  WLGeopointServices.h
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLBaseServices.h"
@import CoreData;

@class WLAnnotationData;
@class WLDetailPoint;

@interface WLGeopointServices : WLBaseServices

-(void)getAnnotations:(void (^)(BOOL succeed, WLAnnotationData *annotationData, NSError *error))completion;

-(void)getAnnotationDetailFromIdPoint:(NSString *)idPoint
                      completionBlock:(void (^)(BOOL succeed, WLDetailPoint *annotationData, NSError *error))completion;

-(BOOL)needCallServices:(NSString*)entityName;

-(BOOL)needCallServicesDetailPoint:(NSString*)entityName
                            withId:(NSString *)idPoint;

+ (instancetype)sharedInstance;

@end
