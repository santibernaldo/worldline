//
//  WLBaseServices.m
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLBaseServices.h"

@implementation WLBaseServices{
    NSString *_baseURL;
}

@synthesize sessionManager = _sessionManager;

#pragma mark - Configurations

-(void)configureSession
{
    self.sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
}


#pragma mark - Properties

-(AFHTTPSessionManager *)sessionManager
{
    if (!_sessionManager)
    {
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:_baseURL]];
    }
    return _sessionManager;
}

#pragma mark - Public Methods

-(void)setupSessionWithUrl:(NSString *)url
{
    _baseURL = url;
    [self configureSession];
}


@end
