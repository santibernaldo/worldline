//
//  WLBaseServices.h
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLApiClient.h"

@interface WLBaseServices : NSObject

@property (nonatomic, strong, readonly) AFHTTPSessionManager *sessionManager;

-(void)setupSessionWithUrl:(NSString *)url;
-(void)configureSession;

@end
