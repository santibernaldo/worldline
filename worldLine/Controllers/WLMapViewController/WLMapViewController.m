//
//  ViewController.m
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLMapViewController.h"

//Data
#import "AGTSimpleCoreDataStack.h"
#import "WLAnnotationData.h"
#import "Location.h"

//Views
#import "WLMapAnnotationView.h"

//Libraries
#import <MapKit/MapKit.h>

//Utils
#import "WLUtils.h"
#import "Config.h"

//Controls
#import "WLAnnotationDetailView.h"

//Services
#import "WLGeopointServices.h"

//Controllers
#import "WLMapDetailViewController.h"

@interface WLMapViewController () <MKMapViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) WLGeopointServices *geoPointServices;
@property (nonatomic, strong) WLAnnotationDetailView *detailView;
@property (strong, nonatomic) AGTSimpleCoreDataStack *coreDataModel;

@end

@implementation WLMapViewController{
    NSString *_stringSearchBar;
    NSArray *_annotations;
    UIActivityIndicatorView *_activity;
    NSString *_idPointTouched;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self baseConfiguration];
}


#pragma mark - Configuration

-(void)baseConfiguration{
    self.title = @"Map View";
    
    //Stack Core Data
    self.coreDataModel = [AGTSimpleCoreDataStack coreDataStackWithModelName:kCOREDATA_MODEL_NAME];
    
    //Search Bar Configuration
     self.searchBar.delegate = self;
    
    //Map Configuration
    self.mapView.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(didTapMap)];
    [self.mapView addGestureRecognizer:tap];

    [self servicesConfiguration];
}

-(void)coreDataFetch{
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:[Location entityName]];

    NSError *error = nil;
    NSArray *results = [self.coreDataModel.context executeFetchRequest:req
                                                         error:&error];
    if (results == nil) {
        NSLog(@"Error al buscar: %@", results);
    }else{
        [self setMapRegion];
        NSMutableArray *array = [NSMutableArray arrayWithArray:results];
        _annotations = [[WLUtils sharedInstance] prepareCoreDataAnnotations:array];
        [self.mapView addAnnotations:_annotations];
    }
}

-(void)servicesConfiguration{
    [WLGeopointServices sharedInstance];
    [[WLGeopointServices sharedInstance] setupSessionWithUrl:kURL_ALL_POINTS];
    
    if ([[WLGeopointServices sharedInstance] needCallServices:[Location entityName]]){
        [self parsePoints];
    }else{
        [self coreDataFetch];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
-(void)parsePoints{
    [self startActivityIndicator];
    [[WLGeopointServices sharedInstance] getAnnotations:^(BOOL succeed, WLAnnotationData *annotationData, NSError *error) {
        
        [self stopActivityIndicator];
        if (succeed){
            
            [self setMapRegion];
            
            _annotations = [[WLUtils sharedInstance] prepareServiceAnnotations:annotationData.arrayAnnotations];
            [self.mapView addAnnotations:_annotations];
          
        }else{
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Info"
                                          message:@"Error handling data"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }

    }];

}

-(void)setMapRegion{
    WLMapAnnotationView *ann = [WLMapAnnotationView new];
    ann.coordinate = CLLocationCoordinate2DMake(41.390205, 2.154007);
    
    MKCoordinateRegion spain = MKCoordinateRegionMakeWithDistance(ann.coordinate,
                                                                  1000000, 1000000);
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(ann.coordinate,
                                                                   10000.5, 10000.5);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.mapView setRegion:spain
                       animated:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mapView setRegion:region
                           animated:YES];
        });
    });

}

#pragma mark - Utils
-(void)startActivityIndicator
{
    _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    _activity.frame = self.view.bounds;
    _activity.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;

    _activity.hidesWhenStopped = YES;
    
    [self.view addSubview:_activity];
    [_activity startAnimating];
}

-(void)stopActivityIndicator
{
    [_activity stopAnimating];
}

#pragma mark - Map Utils
-(void)createMapStringRequest:(NSString *)query
{
    //Remove last annotation
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.detailView removeFromSuperview];
    
    // Create a search request with a string
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    [searchRequest setNaturalLanguageQuery:query];
    
    __block MKMapView *map = self.mapView;
    
    // Create the local search to perform the search
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error)
     {
         
         if (!error)
         {
             for (MKMapItem *mapItem in [response mapItems])
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     if (mapItem == [response.mapItems firstObject]){
                       
                         WLMapAnnotationView * annotation = [[WLMapAnnotationView alloc] initWithLatitude:mapItem.placemark.coordinate.latitude
                                                                                             andLongitude:mapItem.placemark.coordinate.longitude];
                         CLLocation *location = [[CLLocation alloc] initWithLatitude:mapItem.placemark.coordinate.latitude
                                                                           longitude:mapItem.placemark.coordinate.longitude];
                         
                        
                         CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
                         [geocoder reverseGeocodeLocation:location
                                        completionHandler:^(NSArray *placemarks, NSError *error) {
                                            [self stopActivityIndicator];
                                            
                                            CLPlacemark *placemark = [placemarks objectAtIndex:0];
                                            
                                            if ([placemark.administrativeArea isEqualToString:@"Catalonia"]){
                                                [map setCenterCoordinate:annotation.coordinate animated:YES];
                                            }else{
                                                [[WLUtils sharedInstance] attachViewError:self.view text:@"Region doesn't match Catalonia..."];
                                            }
                                            
                                            [map addAnnotations:_annotations];
                                        }];

                     }
                 });
             }
         }
         else
         {
             [self stopActivityIndicator];
             [[WLUtils sharedInstance] attachViewError:self.view text:@"Error loading the data..."];
         }
     }];
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self startActivityIndicator];
    [self.searchBar resignFirstResponder];
    [self createMapStringRequest:_stringSearchBar];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    _stringSearchBar = searchText;
}

#pragma mark - MKMapViewDelegate

-(MKAnnotationView *) mapView:(MKMapView *)mapView
            viewForAnnotation:(id<MKAnnotation>)annotation{
    
    static NSString *pointId = @"pointId";
    
    MKPinAnnotationView *annotationView;
    annotationView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointId];
   
    if (annotationView == nil) {
        // La creamos de cero
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                       reuseIdentifier:pointId];

        annotationView.image = [UIImage imageNamed:@"poi"];
    }
    
    return annotationView;
}

- (void)mapView:(MKMapView *)aMapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    [self.detailView removeFromSuperview];
    
    __block WLMapAnnotationView *annotation;
    [UIView animateWithDuration:0.5 animations:^{
        annotation = (WLMapAnnotationView *)view.annotation;
        _idPointTouched = annotation.idPoint;
        
        [aMapView setCenterCoordinate:annotation.coordinate animated:YES];
    } completion:^(BOOL finished) {
        [self showDetailViewAnnotation:annotation];
    }];
}

-(void)showDetailViewAnnotation:(WLMapAnnotationView *)annotation{
    self.detailView = [[[NSBundle mainBundle] loadNibNamed:@"WLAnnotationDetailView" owner:self options:nil] objectAtIndex:0];
    self.detailView.frame = CGRectMake(0, self.view.bounds.size.height+self.detailView.frame.size.height, self.view.bounds.size.width, self.detailView.frame.size.height);
    self.detailView.delegate = self;
    self.detailView.labelAddress.text = annotation.titlePoint;
    
    [self.view addSubview:self.detailView];
    
    [UIView animateWithDuration:1.0 animations:^{
        self.detailView.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height-self.detailView.frame.size.height/2);
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - WLAnnotationDetailViewDelegate

-(void)showDetail:(WLAnnotationDetailView*)view{
    WLMapDetailViewController *detailVC = [[WLMapDetailViewController alloc] initWithNibName:NSStringFromClass([WLMapDetailViewController class]) bundle:nil];
    detailVC.idPoint = _idPointTouched;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - Tap Selector
-(void)didTapMap{
    [self.detailView removeFromSuperview];
}

@end
