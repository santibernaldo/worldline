#import "DetailPoint.h"
#import "WLDetailPoint.h"

@interface DetailPoint ()

// Private interface goes here.

@end

@implementation DetailPoint

+(instancetype) detailPointWith:(WLDetailPoint *)detailPoint
                        context:(NSManagedObjectContext *) context{
    
    DetailPoint *dp = [NSEntityDescription insertNewObjectForEntityForName:[DetailPoint entityName]
                                                 inManagedObjectContext:context];
    
    dp.address = detailPoint.address;
    dp.detailedDescription = detailPoint.detailedDescription;
    dp.email = detailPoint.email;
    dp.geocoordinates = detailPoint.geocoordinates;
    dp.idPoint = detailPoint.idPoint;
    dp.phone = detailPoint.phone;
    dp.titlePoint = detailPoint.titlePoint;
    dp.transport = detailPoint.transport;
    
    return dp;
}

@end
