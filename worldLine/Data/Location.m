#import "Location.h"
#import "WLLocation.h"

@interface Location ()

// Private interface goes here.

@end

@implementation Location

// Custom logic goes here.
+(instancetype) locationWith:(WLLocation *)location
                     context:(NSManagedObjectContext *) context{
    
    Location *lh = [NSEntityDescription insertNewObjectForEntityForName:[Location entityName]
                                                      inManagedObjectContext:context];
    lh.geocoordinates = location.geocoordinates;
    lh.titlePoint = location.titlePoint;
    lh.idPoint = location.idPoint;
    
    return lh;
}

@end
