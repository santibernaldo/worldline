// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Location.m instead.

#import "_Location.h"

const struct LocationAttributes LocationAttributes = {
	.geocoordinates = @"geocoordinates",
	.idPoint = @"idPoint",
	.titlePoint = @"titlePoint",
};

const struct LocationRelationships LocationRelationships = {
	.detailPoint = @"detailPoint",
};

@implementation LocationID
@end

@implementation _Location

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Point" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Point";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Point" inManagedObjectContext:moc_];
}

- (LocationID*)objectID {
	return (LocationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic geocoordinates;

@dynamic idPoint;

@dynamic titlePoint;

@dynamic detailPoint;

@end

