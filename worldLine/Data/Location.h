#import "_Location.h"

@class WLLocation;

@interface Location : _Location {}
// Custom logic goes here.

+(instancetype) locationWith:(WLLocation *)location
                     context:(NSManagedObjectContext *) context;

@end
