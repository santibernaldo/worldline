//
//  WLMapDetailViewController.h
//  worldLine
//
//  Created by Santi Bernaldo on 06/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLBaseViewController.h"

@interface WLMapDetailViewController : WLBaseViewController

@property (nonatomic, copy) NSString *idPoint;

@end
