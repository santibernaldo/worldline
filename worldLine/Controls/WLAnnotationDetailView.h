//
//  WLAnnotationDetailView.h
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WLAnnotationDetailView;

@protocol WLAnnotationDetailViewDelegate <NSObject>

-(void)showDetail:(WLAnnotationDetailView*)view;

@end

@interface WLAnnotationDetailView : UIView

@property (nonatomic, weak) id<WLAnnotationDetailViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;

@end
