//
//  WLUtils.m
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLUtils.h"
#import "WLMapAnnotationView.h"
#import "Location.h"

@implementation WLUtils

+ (instancetype)sharedInstance
{
    static WLUtils *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[WLUtils alloc] init];
    });
    return _sharedInstance;
}

-(void)attachViewError:(UIView *)view text:(NSString *)aText
{
    CGRect rectView = CGRectMake(0, view.bounds.size.height, view.bounds.size.width, 70);
    CGRect rectViewLabel = CGRectMake(0, 0, view.bounds.size.width, 70);
    UIColor *color = [UIColor colorWithRed:38.0/255.0 green:41.0/255.0 blue:53.0/255.0 alpha:1.0];
    UIView *refreshView = [[UIView alloc]initWithFrame:rectView];
    UILabel *labelError = [[UILabel alloc]initWithFrame:rectViewLabel];
    labelError.font = [UIFont systemFontOfSize:13.0];
    labelError.textColor = [UIColor whiteColor];
    labelError.textAlignment = NSTextAlignmentCenter;
    labelError.text = aText;
    [refreshView addSubview:labelError];
    refreshView.alpha = 0.8;
    refreshView.backgroundColor = color;
    [view addSubview:refreshView];
    [UIView animateWithDuration:1.0 animations:^{
        refreshView.center = CGPointMake(view.bounds.size.width/2, view.bounds.size.height-35);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0
                              delay:1.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             refreshView.center = CGPointMake(view.bounds.size.width/2, view.bounds.size.height + 35);
                         } completion:^(BOOL finished) {
                             [refreshView removeFromSuperview];
                         }];
    }];
}

-(NSArray *)prepareServiceAnnotations:(NSMutableArray *)arrayDict{
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSDictionary *objGeo in arrayDict){
        CGFloat latitude = [[objGeo objectForKey:@"latitude"] floatValue];
        CGFloat longitude = [[objGeo objectForKey:@"longitude"] floatValue];
        
        WLMapAnnotationView *annotation = [[WLMapAnnotationView alloc] initWithLatitude:latitude
                                                                           andLongitude:longitude];
        annotation.idPoint = [objGeo objectForKey:@"idPoint"];
        annotation.titlePoint = [objGeo objectForKey:@"titlePoint"];
        [array addObject:annotation];
    }
    
    return [NSArray arrayWithArray:array];
}

-(NSArray *)prepareCoreDataAnnotations:(NSMutableArray *)arrayLocations{
    NSMutableArray *array = [NSMutableArray array];
    
    for (Location *locationFound in arrayLocations){
        NSArray *points = [locationFound.geocoordinates componentsSeparatedByString: @","];
        
        WLMapAnnotationView *annotation = [[WLMapAnnotationView alloc] initWithLatitude:[[points firstObject] floatValue]
                                                                           andLongitude:[[points objectAtIndex:1] floatValue]];
        annotation.idPoint = locationFound.idPoint;
        annotation.titlePoint = locationFound.titlePoint;
        [array addObject:annotation];
    }
    
    return [NSArray arrayWithArray:array];
}

-(BOOL)checkLocation:(CLLocationCoordinate2D)loc{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    __block BOOL value=NO;
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:loc.latitude longitude:loc.longitude];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
 
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       
                       if ([placemark.administrativeArea isEqualToString:@"Catalonia"])
                           value = YES;
                       
                   }];
    
    return value;
}

@end