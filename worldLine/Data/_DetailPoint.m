// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DetailPoint.m instead.

#import "_DetailPoint.h"

const struct DetailPointAttributes DetailPointAttributes = {
	.address = @"address",
	.detailedDescription = @"detailedDescription",
	.email = @"email",
	.geocoordinates = @"geocoordinates",
	.idPoint = @"idPoint",
	.phone = @"phone",
	.titlePoint = @"titlePoint",
	.transport = @"transport",
};

const struct DetailPointRelationships DetailPointRelationships = {
	.point = @"point",
};

@implementation DetailPointID
@end

@implementation _DetailPoint

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DetailPoint" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DetailPoint";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DetailPoint" inManagedObjectContext:moc_];
}

- (DetailPointID*)objectID {
	return (DetailPointID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic address;

@dynamic detailedDescription;

@dynamic email;

@dynamic geocoordinates;

@dynamic idPoint;

@dynamic phone;

@dynamic titlePoint;

@dynamic transport;

@dynamic point;

@end

