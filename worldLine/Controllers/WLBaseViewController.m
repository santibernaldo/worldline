//
//  WLBaseViewController.m
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLBaseViewController.h"

@interface WLBaseViewController ()

@property (nonatomic, strong) UIView *viewActivity;

@end

@implementation WLBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
- (void)startActivityIndicator{
    if (!self.viewActivity) {
        self.viewActivity=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.viewActivity.backgroundColor=[UIColor whiteColor];
        self.viewActivity.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
        self.activity=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activity.frame = self.viewActivity.bounds;
        self.activity.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
        [self.viewActivity addSubview:self.activity];
        [self.view addSubview:self.viewActivity];
    }
    [self.activity startAnimating];
    self.viewActivity.hidden = NO;
}

- (void)stopActivityIndicator{
    [self.activity stopAnimating];
    self.viewActivity.hidden = YES;
}

@end
