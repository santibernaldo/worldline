//
//  WLAnnotationData.h
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WLAnnotationData : NSObject

@property (nonatomic, strong) NSMutableArray *arrayAnnotations;

+ (instancetype)sharedInstance;

@end
