//
//  WLMapDetailViewController.m
//  worldLine
//
//  Created by Santi Bernaldo on 06/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLMapDetailViewController.h"

//Data
#import "AGTSimpleCoreDataStack.h"
#import "DetailPoint.h"
#import "WLDetailPoint.h"

//Services
#import "WLGeopointServices.h"

//Utils
#import "Config.h"

#define kBOTTOM_MARGIN 100

@interface WLMapDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelTransport;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelPhone;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) AGTSimpleCoreDataStack *coreDataModel;

@end

@implementation WLMapDetailViewController{
    NSString *_addressMail;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self baseConfiguration];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Configuration

-(void)baseConfiguration{
    self.title = @"Point Detail";
    
    [self navBarsButtons];
    
    //Stack Core Data
    self.coreDataModel = [AGTSimpleCoreDataStack coreDataStackWithModelName:kCOREDATA_MODEL_NAME];
    
    [self servicesConfiguration];
}

-(void)servicesConfiguration{
    if ([[WLGeopointServices sharedInstance] needCallServicesDetailPoint:[DetailPoint entityName]
                                                                  withId:self.idPoint]){
        [self parsePoints];
    }else{
        [self coreDataFetch];
    }
}

#pragma mark - Private

-(void)parsePoints{
    [self startActivityIndicator];
    [[WLGeopointServices sharedInstance] getAnnotationDetailFromIdPoint:self.idPoint completionBlock:^(BOOL succeed, WLDetailPoint *annotationData, NSError *error) {
        [self stopActivityIndicator];
        
        _addressMail = annotationData.address;
        
        [self setTextLabel:annotationData.titlePoint label:self.labelTitle];
        [self setTextLabel:annotationData.address label:self.labelAddress];
        [self setTextLabel:annotationData.transport label:self.labelTransport];
        [self setTextLabel:annotationData.email label:self.labelEmail];
        [self setTextLabel:annotationData.phone label:self.labelPhone];
        [self setTexTextView:annotationData.detailedDescription textView:self.textViewDescription];
        [self updateContentSizeScrollView];
    }];
}

-(void)coreDataFetch{
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:[DetailPoint entityName]];
    [req setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idPoint == %@", self.idPoint];
    [req setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [self.coreDataModel.context executeFetchRequest:req
                                                                 error:&error];
    for (DetailPoint *detailPoint in results){
        _addressMail = detailPoint.address;
        
        [self setTextLabel:detailPoint.titlePoint label:self.labelTitle];
        [self setTextLabel:detailPoint.address label:self.labelAddress];
        [self setTextLabel:detailPoint.transport label:self.labelTransport];
        [self setTextLabel:detailPoint.email label:self.labelEmail];
        [self setTextLabel:detailPoint.phone label:self.labelPhone];
        [self setTexTextView:detailPoint.detailedDescription textView:self.textViewDescription];
        [self updateContentSizeScrollView];
    }
}

-(void)updateContentSizeScrollView{
    CGSize textViewSize = [self.textViewDescription sizeThatFits:CGSizeMake(self.textViewDescription.frame.size.width, FLT_MAX)];
    self.textViewDescription.frame = CGRectMake(self.textViewDescription.frame.origin.x, self.textViewDescription.frame.origin.y, textViewSize.width, textViewSize.height);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height+self.textViewDescription.frame.size.height);
}

-(CGSize) getContentSize:(UITextView*) myTextView{
    return [self.textViewDescription sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
}

-(void)setTextLabel:(NSString *)string
              label:(UILabel *)label{
    if ([string isEqualToString:@"null"])
        label.text = @" - ";
    else if (string)
        label.text = string;
    else if (!string)
        label.text = @" - ";
}

-(void)setTexTextView:(NSString *)string
              textView:(UITextView *)textView{
    [textView sizeToFit];
    if ([string isEqualToString:@"null"])
        textView.text = @" - ";
    else if (string)
        textView.text = string;
    else if (!string)
        textView.text = @" - ";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Bar Button Item
-(void)navBarsButtons{
    UIBarButtonItem *one = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share)];
 
    NSArray *buttons = @[one];
    self.navigationItem.rightBarButtonItems = buttons;
}

#pragma mark - Selectors
-(void)share{
    NSString *string = [NSString stringWithFormat:@"Go to this address! \n\n%@",_addressMail];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[string] applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

@end
