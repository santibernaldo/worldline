//
//  WBDMapAnnotationView.h
//  
//
//  Created by Santi Ochoa on 23/09/15.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface WLMapAnnotationView : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *idPoint;
@property (nonatomic, copy) NSString *titlePoint;

-(id)initWithLatitude:(CGFloat)aLatitude
         andLongitude:(CGFloat)aLongitude;

@end
