// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Location.h instead.

@import CoreData;

extern const struct LocationAttributes {
	__unsafe_unretained NSString *geocoordinates;
	__unsafe_unretained NSString *idPoint;
	__unsafe_unretained NSString *titlePoint;
} LocationAttributes;

extern const struct LocationRelationships {
	__unsafe_unretained NSString *detailPoint;
} LocationRelationships;

@class DetailPoint;

@interface LocationID : NSManagedObjectID {}
@end

@interface _Location : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LocationID* objectID;

@property (nonatomic, strong) NSString* geocoordinates;

//- (BOOL)validateGeocoordinates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* idPoint;

//- (BOOL)validateIdPoint:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* titlePoint;

//- (BOOL)validateTitlePoint:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) DetailPoint *detailPoint;

//- (BOOL)validateDetailPoint:(id*)value_ error:(NSError**)error_;

@end

@interface _Location (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveGeocoordinates;
- (void)setPrimitiveGeocoordinates:(NSString*)value;

- (NSString*)primitiveIdPoint;
- (void)setPrimitiveIdPoint:(NSString*)value;

- (NSString*)primitiveTitlePoint;
- (void)setPrimitiveTitlePoint:(NSString*)value;

- (DetailPoint*)primitiveDetailPoint;
- (void)setPrimitiveDetailPoint:(DetailPoint*)value;

@end
