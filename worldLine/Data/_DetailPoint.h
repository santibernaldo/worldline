// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DetailPoint.h instead.

@import CoreData;

extern const struct DetailPointAttributes {
	__unsafe_unretained NSString *address;
	__unsafe_unretained NSString *detailedDescription;
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *geocoordinates;
	__unsafe_unretained NSString *idPoint;
	__unsafe_unretained NSString *phone;
	__unsafe_unretained NSString *titlePoint;
	__unsafe_unretained NSString *transport;
} DetailPointAttributes;

extern const struct DetailPointRelationships {
	__unsafe_unretained NSString *point;
} DetailPointRelationships;

@class Location;

@interface DetailPointID : NSManagedObjectID {}
@end

@interface _DetailPoint : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) DetailPointID* objectID;

@property (nonatomic, strong) NSString* address;

//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* detailedDescription;

//- (BOOL)validateDetailedDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* email;

//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* geocoordinates;

//- (BOOL)validateGeocoordinates:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* idPoint;

//- (BOOL)validateIdPoint:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* phone;

//- (BOOL)validatePhone:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* titlePoint;

//- (BOOL)validateTitlePoint:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* transport;

//- (BOOL)validateTransport:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Location *point;

//- (BOOL)validatePoint:(id*)value_ error:(NSError**)error_;

@end

@interface _DetailPoint (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (NSString*)primitiveDetailedDescription;
- (void)setPrimitiveDetailedDescription:(NSString*)value;

- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;

- (NSString*)primitiveGeocoordinates;
- (void)setPrimitiveGeocoordinates:(NSString*)value;

- (NSString*)primitiveIdPoint;
- (void)setPrimitiveIdPoint:(NSString*)value;

- (NSString*)primitivePhone;
- (void)setPrimitivePhone:(NSString*)value;

- (NSString*)primitiveTitlePoint;
- (void)setPrimitiveTitlePoint:(NSString*)value;

- (NSString*)primitiveTransport;
- (void)setPrimitiveTransport:(NSString*)value;

- (Location*)primitivePoint;
- (void)setPrimitivePoint:(Location*)value;

@end
