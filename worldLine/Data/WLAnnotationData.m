//
//  WLAnnotationData.m
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLAnnotationData.h"

@implementation WLAnnotationData

+ (instancetype)sharedInstance
{
    static WLAnnotationData *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[WLAnnotationData alloc] init];
    });
    return _sharedInstance;
}

@end
