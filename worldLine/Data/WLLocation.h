//
//  WLLocation.h
//  wordLineTest
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 Santi Bernaldo. All rights reserved.
//

#import <JSONModel/JSONModel.h>

/*
 {
 "id": "1",
 "title": "Casa Batlló",
 "geocoordinates": "41.391926,2.165208"
 }
 */

@interface WLLocation : JSONModel

@property (nonatomic, strong) NSString *idPoint;
@property (nonatomic, strong) NSString *geocoordinates;
@property (nonatomic, strong) NSString *titlePoint;
@property (nonatomic) NSNumber <Optional> *latitude;
@property (nonatomic) NSNumber <Optional> *longitude;

@end
