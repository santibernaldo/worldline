//
//  Config.h
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#define kURL_ALL_POINTS         @"http://t21services.herokuapp.com/points"
#define kURL_POINT              @"http://t21services.herokuapp.com/points/"
#define kCOREDATA_MODEL_NAME    @"Model"
