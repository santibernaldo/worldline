//
//  WLDetailPoint.m
//  wordLineTest
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 Santi Bernaldo. All rights reserved.
//

#import "WLDetailPoint.h"

@implementation WLDetailPoint

+ (JSONKeyMapper*)keyMapper {
    NSDictionary *map = @{@"id" : @"idPoint",
                          @"title" : @"titlePoint",
                          @"address" : @"address",
                          @"transport" : @"transport",
                          @"email" : @"email",
                          @"geocoordinates" : @"geocoordinates",
                          @"description" : @"detailedDescription",
                          @"phone" : @"phone"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

@end
