//
//  WLBaseViewController.h
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLBaseViewController : UIViewController

@property (strong, nonatomic) UIActivityIndicatorView *activity;
- (void)startActivityIndicator;
- (void)stopActivityIndicator;

@end
