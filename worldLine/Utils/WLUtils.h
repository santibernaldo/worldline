//
//  Utils.h
//  
//
//  Created by Santi Ochoa on 27/10/15.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class WLMapAnnotationView;

@interface WLUtils : NSObject

+ (instancetype)sharedInstance;

-(void)attachViewError:(UIView *)view text:(NSString *)aText;

-(NSArray *)prepareServiceAnnotations:(NSMutableArray *)arrayDict;
-(NSArray *)prepareCoreDataAnnotations:(NSMutableArray *)arrayLocations;

-(BOOL)checkLocation:(CLLocationCoordinate2D)location;

@end
