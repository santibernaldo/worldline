//
//  WLAnnotationDetailView.m
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLAnnotationDetailView.h"

@interface WLAnnotationDetailView ()

- (IBAction)showDetail:(id)sender;

@end

@implementation WLAnnotationDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)showDetail:(id)sender {
    [self.delegate showDetail:self];
}
@end
