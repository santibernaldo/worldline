//
//  WBDMapAnnotationView.m
//  
//
//  Created by Santi Ochoa on 23/09/15.
//
//

#import "WLMapAnnotationView.h"
#import <CoreLocation/CoreLocation.h>

@implementation WLMapAnnotationView

-(id)initWithLatitude:(CGFloat)aLatitude
         andLongitude:(CGFloat)aLongitude{
    if (self = [super init]) {
        _coordinate = CLLocationCoordinate2DMake(aLatitude, aLongitude);
    }
    return self;
}

@end
