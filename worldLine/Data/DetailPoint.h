#import "_DetailPoint.h"

@class WLDetailPoint;

@interface DetailPoint : _DetailPoint {}
// Custom logic goes here.

+(instancetype) detailPointWith:(WLDetailPoint *)detailPoint
                        context:(NSManagedObjectContext *) context;

@end
