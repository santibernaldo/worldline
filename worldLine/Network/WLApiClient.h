//
//  WLApiClient.h
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface WLApiClient : AFHTTPSessionManager

@end
