//
//  WLGeopointServices.m
//  worldLine
//
//  Created by Santi Bernaldo on 05/11/15.
//  Copyright (c) 2015 WorldLine. All rights reserved.
//

#import "WLGeopointServices.h"

//Data
#import "AGTSimpleCoreDataStack.h"
#import "WLAnnotationData.h"
#import "WLLocation.h"
#import "Location.h"
#import "WLDetailPoint.h"
#import "DetailPoint.h"

//Utils
#import "Config.h"

//Map Annotation
#import "WLMapAnnotationView.h"

@interface WLGeopointServices ()

@property (strong, nonatomic) AGTSimpleCoreDataStack *coreDataModel;

@end

@implementation WLGeopointServices

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.coreDataModel = [AGTSimpleCoreDataStack coreDataStackWithModelName:kCOREDATA_MODEL_NAME];
    }
    return self;
}

+ (instancetype)sharedInstance
{
    static WLGeopointServices *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[WLGeopointServices alloc] init];
    });
    return _sharedInstance;
}

-(void)getAnnotations:(void (^)(BOOL succeed, WLAnnotationData *annotationData, NSError *error))completion{
    
    [self.sessionManager GET:kURL_ALL_POINTS parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject)
        {
            WLAnnotationData *resultsData = [[WLAnnotationData alloc] init];
        
            id listData = [responseObject objectForKey:@"list"];
            if ([listData isKindOfClass:[NSArray class]])
            {
                NSMutableArray *resultsArray = [[NSMutableArray alloc] init];
                for (NSMutableDictionary *obj in listData) {
                    
                    NSDictionary *dictObj = [NSDictionary dictionaryWithDictionary:obj];
                    NSError* err = nil;
                    
                    WLLocation *location = [[WLLocation alloc] initWithDictionary:dictObj error:&err];
                    
                    //Map JSONModel into Core Data
                    [Location locationWith:location context:self.coreDataModel.context];
                    
                    NSDictionary *dictGeo = [self getDictionaryFrom:location];
                    [resultsArray addObject:dictGeo];
                }
                resultsData.arrayAnnotations = resultsArray;

                if (resultsData){
                    [self save];
                    
                    completion(YES, resultsData, nil);
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(NO, nil, error);
    }];
}

-(void)getAnnotationDetailFromIdPoint:(NSString *)idPoint
completionBlock:(void (^)(BOOL succeed, WLDetailPoint *annotationData, NSError *error))completion{
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@",kURL_POINT,idPoint];

    [self.sessionManager GET:urlPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject)
        {
            WLDetailPoint *detailPointData;
            
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dictObj = [NSDictionary dictionaryWithDictionary:responseObject];
                NSError* err = nil;
                
                detailPointData = [[WLDetailPoint alloc] initWithDictionary:dictObj error:&err];
                
                //Map JSONModel into Core Data
                [DetailPoint detailPointWith:detailPointData context:self.coreDataModel.context];
        
                if (detailPointData){
                    
                    [self save];
                    
                    completion(YES, detailPointData, nil);
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completion(NO, nil, error);
    }];
}

-(void)save{
    [self.coreDataModel saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar %s \n\n %@", __func__, error);
    }];
}

#pragma mark - Private
-(NSDictionary *)getDictionaryFrom:(WLLocation*)location{
    NSArray *points = [location.geocoordinates componentsSeparatedByString: @","];
    
    return [NSDictionary dictionaryWithObjectsAndKeys:
            [points firstObject],@"latitude",
            [points objectAtIndex:1],@"longitude",
            location.idPoint,@"idPoint",
            location.titlePoint,@"titlePoint",nil];
}

-(BOOL)needCallServices:(NSString*)entityName{
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:entityName];
   
    NSError *error = nil;
    NSArray *results = [self.coreDataModel.context executeFetchRequest:req
                                                                 error:&error];
    if (results.count == 0) {
        return YES;
    }else{
        return NO;
    }
}

-(BOOL)needCallServicesDetailPoint:(NSString*)entityName
                            withId:(NSString *)idPoint{
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [req setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idPoint == %@", idPoint];
    [req setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [self.coreDataModel.context executeFetchRequest:req
                                                                 error:&error];
    if (results.count == 0) {
        return YES;
    }else{
        return NO;
    }
}

@end
